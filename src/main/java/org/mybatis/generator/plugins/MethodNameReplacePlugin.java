package org.mybatis.generator.plugins;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.internal.util.StringUtility;

import java.util.List;

/**
 * @author: z_dd
 * @date: 2022/2/25 16:55
 * @Description: 生成Java方法名替换插件
 */
public class MethodNameReplacePlugin extends PluginAdapter {

    private boolean switchOn; // 应用插件开关
    private String suffix;

    public MethodNameReplacePlugin(boolean switchOn, String suffix) {
        this.switchOn = switchOn;
        this.suffix = suffix;
    }

    public MethodNameReplacePlugin() {
    }

    @Override
    public boolean validate(List<String> warnings) {
        String switchOn_Proper = properties.getProperty("switchOn");
        if (StringUtility.stringHasValue(switchOn_Proper)) {
            this.switchOn = Boolean.valueOf(switchOn_Proper);
        }
        String suffix_Proper = properties.getProperty("suffix");
        if (StringUtility.stringHasValue(suffix_Proper)) {
            this.suffix = suffix_Proper;
        }

        return true;
    }

    @Override
    public boolean clientInsertMethodGenerated(Method method, Interface interfaze,
                                               IntrospectedTable introspectedTable) {
        method.setName(method.getName() + this.suffix);

        return true;
    }

    public boolean isSwitchOn() {
        return switchOn;
    }

    public void setSwitchOn(boolean switchOn) {
        this.switchOn = switchOn;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}
